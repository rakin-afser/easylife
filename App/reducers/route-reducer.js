import { Reducer , ActionConst, Actions} from 'react-native-router-flux';

const defaultReducer = Reducer();

export default (state, action)=> {
  console.log(' Reducing action: ', action.type);
  switch (action.type) {
    case ActionConst.FOCUS:
      console.log('Focus Event fired ', action.routeName);
      return defaultReducer(state, action);
    default:
    console.log('default action  ', action.routeName);
      return defaultReducer(state, action);

  }
}
