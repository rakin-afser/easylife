import {hashMap} from 'mori';
const INITIAL_STATE = hashMap();
import {ADD_TOKEN, ADD_USER, ADD_TAB } from '../constants';
import * as appReducerAction from '../action/appReducerAction';

export default (state = hashMap(), action) => {
  switch (action.type) {
    case ADD_TOKEN:
        return appReducerAction.addToken(state, action);
        break;
    case ADD_USER:
        return appReducerAction.addUser(state, action);
        break;
    case ADD_TAB:
        return appReducerAction.addTab(state, action);
        break;
    default:
      return state;
  }
}
