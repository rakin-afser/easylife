import {INITIAL_MEAL_TABS} from '../constants';

export const initialCommonDataStore = (type, payloads) => ({type: type, payloads : payloads});
