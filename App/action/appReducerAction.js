import {assoc} from 'mori';
export const addToken = (state, action) => {
  return assoc(state, 'token', action.payload)
}
export const addUser = (state, action) => {
  return assoc(state, 'user', action.payload);
}
export const addTab = (state, action) => {
  return assoc(state, 'tabs', action.payload);
}
