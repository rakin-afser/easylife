import * as constants from '../constants';

export function signIn(userId, password) {
  return {
    type: constants.LOGIN,
    payload: {
      userId: userId,
      password: password
    }
  }
}
