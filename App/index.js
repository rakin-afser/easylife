import React from 'react';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider, connect } from 'react-redux';
import { Scene, Actions, Router } from 'react-native-router-flux';

import Home from './Scenes/home';
import Login from './Scenes/Login';
import MessTab from './Scenes/Tab/MessTab';
import MyBazar from './Scenes/Tab/BazarTab';
import BottomTab from './Scenes/Tab';

import { persistStore, autoRehydrate } from 'redux-persist';
import { AsyncStorage, StyleSheet} from 'react-native'

import {initialMealTabs} from './helper/initialTask';

// Helper Functions
import {dynamicTabs} from './helper/routingHelper';
import remoteActionMiddlewareSocket , {apiMiddleware} from './helper/middleware';
const style = StyleSheet.create({
  main:{
    flex: .8
  },
  tab: {
    flex: .2
  }
})

var initialTab = [
  {key: 'messTab', component: MessTab, title:' Mess'},
  {key: 'bazarTab', component: MyBazar, title:' MyBazar'}
];

const navigator = Actions.create(
  <Router hideNavBar>
    <Scene key='root' hideNavBar  >
      <Scene key='login' component={Login} initial/>
      <Scene key='home' component={Home}  />
      <Scene key='bottomTab' component={BottomTab}  />
    </Scene>
    <Scene key='tabManager'  hideNavBar={true} tabs={true} direction={'horizontal'} tabBarPosition={'bottom'}   >
        {
          initialTab.map(function(item, index) {
            return (
              <Scene key={item.key}  component={item.component} hideNavBar title={item.title} />
            )
          })
        }
    </Scene>
  </Router>
)
// <Scene key='mess'  component={MessTab} hideNavBar title='Meal' />
// <Scene key='myBazar'  component={Login}   title='Login' hideNavBar   />

console.disableYellowBox = true; // disable disableYellowBox
const ReduxRouter = connect((state) => ({
  state: state.route
}))(Router);

var socket;
const reducers = require('./reducers').default;
const store = compose(autoRehydrate())(createStore)(reducers, applyMiddleware(remoteActionMiddlewareSocket(socket), apiMiddleware));


//Initial Task
initialMealTabs(store)


store.subscribe(()=>{
  console.log(' Logger -> Store Updated -> ',store.getState());
})

persistStore(store, {storage: AsyncStorage}, (items) => {
  console.log('restored')
})


export default class App extends React.Component {
  render(){
    return (
      <Provider store={store} >
        <ReduxRouter navigator={navigator} />
      </Provider>
    )
  }
}
