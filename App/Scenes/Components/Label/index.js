import React, {Component} from 'react';
import { Text, StyleSheet} from 'react-native';

export const Label = (props) => {

  return (
      <Text
          style={props.styles && props.styles.textLabel ? props.styles.textLabel : Style.textLabel}
      >
          {props.text}
      </Text>
  )
}

const Style = StyleSheet.create({
  textLabel: {
        fontSize: 20,
        fontWeight: 'bold',
        fontFamily: 'Verdana',
        marginBottom: 10,
        color: '#595856'
    }
})
