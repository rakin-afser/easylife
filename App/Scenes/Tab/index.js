import React, {Component} from 'react';
import {ScrollView, Text, View, TextInput, Alert} from 'react-native';
import {Container, Header, Left, Content, Button, Tabs, Tab, TabHeading, Icon, Footer, FooterTab,} from 'native-base';
import {bindActionCreators} from 'redux';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';

//Import components
//import {Container, Button, Label} from '../../Components';
import style from './MessTab/style';
//import Icon from 'react-native-vector-icons/FontAwesome';
import * as actions from '../../action/mealAction';
export class BottomTab extends Component {
  constructor(props) {
    super(props)
    console.log('---$---', this.props.title);
  }
  press() {
    Actions.pop()
  }
  render(){
    var _that = this;
    return (

      <Container>
        <Header androidStatusBarColor="#00523E"  style={{backgroundColor:"#00C694" }} hasTabs>
          <Left style={style.headerTitle}>
            <Text style = {style.headerText}>{this.props.title}</Text>
          </Left>

        </Header>
        <Tabs onChangeTab={({i, ref, from})=> console.log(i, from)} tabBarUnderlineStyle={style.tabs} >
          {
            ['pizza','calculator','clock'].map(function(item, index) {
              return (
                <Tab key={index} heading ={<TabHeading  style={style.tab}><Icon name={item} /></TabHeading>} >
                    <Text>{index}</Text>
                </Tab>
              )

            })
          }
        </Tabs>
        <Content>
          <Button block info onPress={()=>{ Actions.pop()}} >
            <Text> Back </Text>
          </Button>
        </Content>
        <Footer>
          <FooterTab>
            <Button >
              <Icon  name="apps" />
            </Button>
            <Button active>
              <Icon active name="camera" />
            </Button>
          </FooterTab>
        </Footer>
      </Container>

    )
  }
}


function mapStateToProps(state) {
  return ({
    state:state
  })
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch)
}
BottomTab.propTypes = {
  title : React.PropTypes.string.isRequired
}
export default connect(mapStateToProps, mapDispatchToProps)(BottomTab)
