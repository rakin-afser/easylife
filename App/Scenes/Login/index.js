import React, {Component} from 'react';
import {ScrollView, Text, View, TextInput, Alert} from 'react-native';
import {bindActionCreators} from 'redux';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';

//Import components
import {Container, Button, Label} from '../Components';
import style from './style';
//import Icon from 'react-native-vector-icons/FontAwesome';
import * as actions from '../../action/authAction';
export class Login extends Component {
  constructor(props) {
    super(props)
    //this._onPressButton = this._onPressButton.bind(this);
  }
  signIn() {
    this.props.signIn(this.state.userId, this.state.password);
  }
  render(){
    var _that = this;
    return (
      <ScrollView style={style.scroll}>
        <Container>
          <Button
            label="Forgot Login/Pass"
            styles={{button: style.alignRight, label: style.label}}
             ></Button>
        </Container>
        <Container>
          <Label text="Username or Email" />
          <TextInput
              style={style.textInput}
              onSubmitEditing ={(event)=>{
                    this.setState({
                      'userId': event.nativeEvent.text
                    })
                  }} placeholder='User ID OR Email'
                  onChangeText={(text)=> {
                    this.setState({
                      userId: text
                    })
                  }}
          />
        </Container>
        <Container>
          <Label text="Password" />
          <TextInput
              secureTextEntry={true}
              style={style.textInput}
              onSubmitEditing ={(event)=>{
                    this.setState({
                      'password': event.nativeEvent.text
                    })
                  }} placeholder='Password'
                  onChangeText={(text)=> {
                    this.setState({
                      password: text
                    })
                  }}
          />
        </Container>
        <Container>
          <Button onPress={() => {

          }}
          styles={{button: style.transparentButton}}
          >
            <View style={style.inline} >

              <Text style={[style.buttonBluetext, style.buttonBigText]} > Connect</Text>
              <Text style={style.buttonBluetext}> with Facebook</Text>
            </View>
          </Button>
        </Container>
        <Container>
          <Button
          label='Sign-In'
          onPress={this.signIn.bind(this)}
          styles={{button:style.primaryButton, label: style.buttonWhiteText}}
          />
        </Container>
        <Container>
          <Button
          onPress={()=> {
            this.setState({
              userId :'',
              password : ''
            })
          }}
          label='CANCEL'
          styles={{label: style.buttonBlackText}}
          />
        </Container>
      </ScrollView>
    )
  }
}


function mapStateToProps(state) {
  return ({
    state:state
  })
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)

// <View style={{
//   flex:1,
//   flexDirection:'column',
//   justifyContent:'center',
//   alignSelf: 'auto'
// }}>
//   <View style={{flex:2, backgroundColor:'skyblue', justifyContent: 'center', alignItems: 'center'}} >
//     <Button onPress={this._onPressButton} title='Hit Me' color='#ddd' />
//   </View>
//   <View style={{flex:2, backgroundColor:'red'}} />
//   <View style={{width: '60%', flex:2,backgroundColor:'green', justifyContent: 'center', alignItems: 'center', margin:'20%'}} >
//     <TextInput onChangeText={(text)=>{
//       console.log(text);
//     }} onSubmitEditing ={(event)=>{
//       console.log('--- ',event.nativeEvent.text);
//     }} placeholder='Type here'
//     style={{width:'100%'}}
//     />
//   </View>
// </View>
