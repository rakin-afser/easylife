import {ADD_TOKEN, ADD_USER, ADD_TAB, API, LOGIN } from '../../constants';
import {Actions} from 'react-native-router-flux';


import {toJs} from 'mori';
export default async (store, dispatch, action) => {
  if(action.type === LOGIN) {
    var response = await fetchLogin(action.payload);
    if(response.message == 'ok') {
      dispatch({
        type: ADD_TOKEN,
        payload : response.token
      })
      dispatch({
        type : ADD_USER,
        payload : response.user
      })
      dispatch({
        type : ADD_TAB,
        payload : response.tabs
      })

      Actions.bottomTab();
    }

  }
}

const fetchLogin = (data) => {
  const reqObj =  {
      method : 'POST',
      headers : { 'Content-Type':'application/x-www-form-urlencoded' ,'Access-Control-Allow-Origin':'*' },
      body: `userId=${data.userId}&password=${data.password}`

    }
  return fetch(API.concat('/login'),reqObj)
  .then(function(response){
    //console.log('response ', response.json());
    return response.json()
  })
  .catch(function(err){
    console.error('Error -> FetchLogin -> ', err);
  })
}
