
export const mapBinder = (mainObj, key, value) => {
  try {
    if(typeof mainObj == 'undefined' ) {
      return Object.assign({}, {[key] : value})
    } else if (typeof mainObj == 'object') {
      return Object.assign(mainObj, {[key] : value})
    }
  } catch (e) {
    console.warn('Map Type Object Builder failed - Path App/helper/mapType -> mapBinder -> ', e);
  }
}
