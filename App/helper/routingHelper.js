import {Scene} from 'react-native-router-flux';

export const dynamicTabs = (key, Component, hideNavBar, title) => {
  return (<Scene key={key} hideNavBar = {hideNavBar} title = {title} component={Component} />)
}
